# Sass

This is the second version of my homepage using [Sass](https://sass-lang.com) as _CSS_ preprocessor. Actually is the CSS
of the page not that much, that there is a real need, but I think it's a nice next step to also introduce some other
tools for frontend development.

## Introduction to Sass
At first we will discuss what _Sass_ can do for us. It can help us writing CSS by adding features like _variables_,
_nesting_, _partials_, _modules_, _mixins_, _inheritance_ and _operators_. We will go in detail for some of them later.

That sounds nice, but since the browser still only understands _CSS_ we need a way to transform our `scss` files into
`css` files. Therefore we need to install _Sass_. There are many way to do this. We will use the _Node Package Manager_
(`npm`). This tool is part of [Node.js](https://nodejs.org) which is a runtime environment for _JavaScript_.

So let's download _Node.js_ from [nodejs.org](https://nodejs.org/en/download/) and run the installer. Afterwards we
should be able install _Sass_ by running:

    npm install -g sass

_Sass_ is now installed _globally_ which means we can run it from everywhere. So we can now write our `scss` files and
translate them to a `css` file by running:

    sass input.scss output.css

or in our case (having the `scss` files in a `sass` folder and the output in the `css` folder):

    sass sass/main.scss css/main.css

## Used technologies

### Node.js / npm

### Sass

## Links
* [Name that Color](http://chir.ag/projects/name-that-color)
